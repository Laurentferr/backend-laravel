<?php

use Illuminate\Database\Seeder;
use App\Famille;

class FamillesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $famille = new Famille;
        $famille->nom = 'T-Shirt';
        $famille->slug = 'TEE';
        $famille->save();

        $famille = new Famille;
        $famille->nom = 'Chemises';
        $famille->slug = 'CHE';
        $famille->save();

        $famille = new Famille;
        $famille->nom = 'Pantalons';
        $famille->slug = 'PAN';
        $famille->save();

        $famille = new Famille;
        $famille->nom = 'Shorts';
        $famille->slug = 'SHO';
        $famille->save();

        $famille = new Famille;
        $famille->nom = 'Chaussures';
        $famille->slug = 'CHA';
        $famille->save();
    }
}
