<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new Product;
        $product->nom = 'T-Shirt à manches longues';
        $product->prix = '19';
        $product->famille_id = 1;
        $product->save();

        $product = new Product;
        $product->nom = 'T-Shirt à manches courtes';
        $product->prix = '39';
        $product->famille_id = 1;
        $product->save();

        $product = new Product;
        $product->nom = 'Chemise Hawaienne';
        $product->prix = '39';
        $product->famille_id = 2;
        $product->save();

        $product = new Product;
        $product->nom = 'Chemise blanche';
        $product->prix = '39';
        $product->famille_id = 2;
        $product->save();

        $product = new Product;
        $product->nom = 'Jean';
        $product->prix = '59';
        $product->famille_id = 3;
        $product->save();

        $product = new Product;
        $product->nom = 'Pantalon en toile';
        $product->prix = '39';
        $product->famille_id = 3;
        $product->save();

        $product = new Product;
        $product->nom = 'Chaussures de baskets';
        $product->prix = '35';
        $product->famille_id = 4;
        $product->save();

        $product = new Product;
        $product->nom = 'Savates 2 doigts';
        $product->prix = '4';
        $product->famille_id = 4;
        $product->save();
    }
}
