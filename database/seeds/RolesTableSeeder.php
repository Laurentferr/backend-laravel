<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role;
        $role->nom = 'Admin';
        $role->description = "Administrateur de l'application, voit toutes les pages";
        $role->save();

        $role = new Role;
        $role->nom = 'User';
        $role->description = "Utilisateur inscrit, ne voit pas les pages Admin";
        $role->save();
    }
}
