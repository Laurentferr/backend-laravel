<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //dd($role_admin, $role_user);

        $user = new User;
        $user->firstname = 'Laurent';
        $user->lastname = 'Ferreol';
        $user->email = 'artisandigital66@gmail.com';
        $user->password = bcrypt('password');
        $user->remember_token = 'CoSnSIfzIlEMgQ6TrsYSkpcE3P20DX1PRyzjrmSUBtZ30dhNNKfyVSI2PBk9';
        $user->save();
        $user->roles()->attach(Role::where('nom', 'Admin')->first());

        $user = new User;
        $user->firstname = 'Laurent';
        $user->lastname = 'Ferreol';
        $user->email = 'laurent.ferreol@orange.fr';
        $user->password = bcrypt('password');
        $user->remember_token = '';
        $user->save();
        $user->roles()->attach(Role::where('nom', 'User')->first());git
        
    }
}
