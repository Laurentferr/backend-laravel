<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Famille;

class FamillesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $familles = Famille::all();
        //dd($familles);

        return view('familles', ['familles' => $familles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('formulaires/familles/famille_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'nom' => 'required|max:255',
            'slug' => 'required',
        ]);

        $famille = new Famille;
        $famille->nom = $request['nom'];
        $famille->slug = $request['slug'];
        $famille->save();

        // écriture dans la variable de session pour afficher le message de feedback 
        session()->flash('notification.message', 'Famille ajoutée avec succès');
        session()->flash('notification.type', 'success');

        // Redirection vers une action du controlleur
        return redirect()->action("FamillesController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);

        $famille = Famille::where('id', $id)->first();

        return view ('formulaires.familles.famille_update', ['famille' => $famille]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validation des données
        $request->validate([
            'nom' => 'required|max:255',
            'slug' => 'required|max:4',
            
        ]);

        //  Mise a jour de la table
        $famille = Famille::where('id', $id)->first();
        //dd($famille);
        $famille->nom = $request['nom'];
        $famille->slug = $request['slug'];
        $famille->save();

        session()->flash('notification.message', 'Famille modifiée avec succès');
        session()->flash('notification.type', 'success');

        return redirect()->action("FamillesController@index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        //dd($id);

        Famille::where('id', $id)
                    ->delete();

        session()->flash('notification.message', 'Famille supprimée avec succès');
        session()->flash('notification.type', 'danger');

        return redirect()->action("FamillesController@index");

    }
}
