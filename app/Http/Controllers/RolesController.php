<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class RolesController extends Controller
{
    public function index() 
    {
        $users = User::all();

        return view ('roles', ['users' => $users]);
    }

    public function assignRoles(Request $request, $id) 
    {

        $user = User::where('id', $id)->first();
        
        $user->roles()->detach();
        
        if ($request['role_user']) {
            $user->roles()->attach(Role::where('nom', 'User')->first());
        }

        if ($request['role_admin']) {
            $user->roles()->attach(Role::where('nom', 'Admin')->first());
        }

        return redirect()->action("RolesController@index");
    }
}
