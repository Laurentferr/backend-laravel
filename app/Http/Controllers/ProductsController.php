<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\Famille;

class ProductsController extends Controller
{
    // Simple Retrieve avec la façade DB (Sans faire appel au Modèle)
    // $produit = DB::table('products')->find(1);
    // Il faut importer la façade DB avec: use DB;

    public function index1() {

        // on utilise le Modèle pour afficher le nom de la famille
        $produit = Product::find(1);
        // dd($produit);
        return view('page1', ['produit' => $produit]);
    }

    // Retrieve filtré avec le Modèle
    // Il faut au préalable importer le modele avec: use App\Product;

    public function index() {

        $produits = Product::orderBy('id', 'asc')->get();

        return view('products', ['produits' => $produits]);

    }

    // Retrieve avec utilisation du Modèle Product
        //    Créer le modele : php artisan make:model Product
        //  importer le modéle dans le controller: use App\Product;
        //  $produits = Product::all();      cas du retrieve de toute la table
        //  $produit = Product::find(1);     cas du retrieve d'une ligne par son id
    
    public function create() {

        $familles = Famille::all();
        // dd($familles);

        return view('formulaires.produits.product_create_form', ['familles' => $familles]);
    }

    public function store(Request $request) {

        // dd($request->all());

        $request->validate([
            'nom' => 'required|max:255',
            'prix_achat' => 'required',
            'prix_vente' => 'required',
            'famille' => 'required',
            'stock' => 'required',
        ]);

        $product = new Product;
        $product->nom = $request['nom'];
        $product->prix_achat = $request['prix_achat'];
        $product->prix_vente = $request['prix_vente'];
        $product->famille_id = $request['famille'];  // On écrit maintenant l'id de la famille
        $product->stock = $request['stock'];
        $product->save();

        // écriture dans la variable de session pour afficher le message de feedback 
        session()->flash('notification.message', 'Produit ajouté avec succès');
        session()->flash('notification.type', 'success');

        // Redirection vers une action du controlleur
        return redirect()->action("ProductsController@index");
    }

    public function destroy($id) {

        //dd($id);

        Product::where('id', $id)
                    ->delete();

        session()->flash('notification.message', 'produit supprimé avec succès');
        session()->flash('notification.type', 'danger');

        return redirect()->action("ProductsController@index");

    }

    // Affichage du formulaire d'édition, on passe la variable $product identifié par son $id

    public function edit($id) {

        
        $familles = Famille::all();
        $product = Product::where('id', $id)->first();
        //dd($product, $familles);

        return view ('formulaires.produits.product_update_form', ['product' => $product, 'familles' =>$familles]);
    }

    //Enregistrement du formulaire d'édition

    public function update(Request $request, $id) {
        //dd($request);
        // Validation des données
        $request->validate([
            'nom' => 'required|max:255',
            'prix_achat' => 'required',
            'prix_vente' => 'required',
            'famille' => 'required',
            'stock' => 'required',
        ]);

        //  Mise a jour de la table
        $product = Product::where('id', $id)->first();
        // dd($product);
        $product->nom = $request['nom'];
        $product->prix_achat = $request['prix_achat'];
        $product->prix_vente = $request['prix_vente'];
        $product->famille_id = $request['famille'];
        $product->stock = $request['stock'];
        $product->save();

        session()->flash('notification.message', 'Produit modifié avec succès');
        session()->flash('notification.type', 'success');

        return redirect()->action("ProductsController@index");

    }

    public function boutique() {

        $produits = Product::paginate(1);
        $count = Product::get()->count();
        // dd($produits);
        return view('boutique', ['produits' => $produits, 'count' => $count]);
    }
}
