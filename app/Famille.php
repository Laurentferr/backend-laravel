<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Famille extends Model
{
    // on établit la relation 1 to Many avec la table produits
    
    public function products() {

        return $this->hasMany('App\Product');
    }
}
