@extends('layout', ['title' => 'Droits insuffisants'])

@section('content')
<div class="container">
<br>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-header bg-danger text-light">DROITS INSUFFISANTS !</div>

                <div class="card-body bg-danger text-light text-center">
                    <p class="mt-4 mb-5">
                        Vous n'avez pas l'autorisation d'accès à cette page,<br>
                    </p>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection