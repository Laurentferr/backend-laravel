@extends('layout')

@section('content')
<div class="row text-center">
    <div class="col-12 col-sm-10 justify-content-center mx-auto">
        <h1>Produits</h1>
        <h5>Retrieve de toute la table 'products'</h5>
        
        <table class="table table-responsive table-hover">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prix d'achat</th>
                    <th scope="col">Prix de vente</th>
                    <th scope="col">Famille</th>
                    <th scope="col">Stock</th>
                </tr>
            </thead>
            @foreach($produits as $produit)
            <tbody>
                <tr>
                    <th>{{ $produit->id }}</th>
                    <td>{{ $produit->nom }}</td>
                    <td>{{ $produit->prix_achat }} €</td>
                    <td>{{ $produit->prix_vente }} €</td>
                    <td>{{ $produit->famille->nom }}</td> {{-- Le nom de la famille est récupéré par la fonction famille() --}}
                    <td>{{ $produit->stock }}</td>
                    <td>
                        <a class="btn btn-danger btn-sm" href="product/delete/{{ $produit->id }}" role="button" onclick="return confirm('Etes vous sûr de vouloir supprimer ce produit ?')">Supprimer<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                    <td>
                        <a class="btn btn-warning btn-sm" href="product/edit/{{ $produit->id }}" role="button">Modifier<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                </tr>
            </tbody>
            @endforeach
        </table>
        <div class="mx-auto">
            <a role="button" class="btn btn-success btn-sm" href="/product/create">Ajouter un Produit</a>
        </div>
    
    </div>
    <br>
</div>

@endsection