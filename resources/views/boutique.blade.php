@extends('layout')

@section('content')
    <div class="row justify-content-center mt-3">
        
            <p class="justify-content-center">{{ $produits->links() }}</p>
        
    </div>
    
    
    @foreach($produits as $produit)
        <div class="col-8 mx-auto">
            <div class="card">
                <div class="card-header">
                    <p class="text-left">{{ $produit->nom }}</p><p class="text-right">{{ $produit->famille->nom }}</p> 
                </div>
                <div class="card-body">
                <div class="text-left">
                    {{ $produit->prix }} €
                </div>
                <div class="text-right">
                    <a href="#" role="button" class="btn btn-primary btn-sm text-right">Ajouter au Panier</a>
                </div>
                </div>
            </div>
        </div>
        
    @endforeach
    <div class="col-8 mx-auto">
        <p>Il y a actuellement {{ $count }} {{ str_plural('produit') }} en vente</p>
    </div>
@endsection