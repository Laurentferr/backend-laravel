@extends('layout')

@section('content')
<div class="row">
    <div class="col-12 text-center">
        <div class="card">
            <div class="card-header">
                <h1>Affichage d'un seul produit</h1>
            </div>
            <div class="card-body">
                @if($produit)
                    <h3 class="text-left pl-4">id: {{ $produit->id }}</h3>
                    <hr>
                    <h3 class="text-left pl-4">Nom: {{ $produit->nom }}</h3>
                    <hr>
                    <h3 class="text-left pl-4">Prix: {{ $produit->prix }}</h3>
                    <hr>
                    <h3 class="text-left pl-4">ID Famille: {{ $produit->famille_id }}</h3>
                    <hr>
                    <h3 class="text-left pl-4">Famille: {{ $produit->famille->nom }}</h3>
                @else
                    <h3>Pas de produit a afficher</h3>
                @endif
            </div>
        </div>
        
        
            
            
            
        
        
    </div>
</div>

@endsection