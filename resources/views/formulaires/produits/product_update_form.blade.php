@extends('layout')

@section('content')

<form class="form-horizontal" action="{{ url('product/update/' . $product->id) }}" method="POST">
@csrf
@method('PATCH')

  <div class="form-group">
    <label for="nom">Nom</label>
    <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom du produit" value="{{ $product->nom }}">
  </div>

    @if ($errors->has('nom'))
    <span class="help-block">
        <strong class='text-danger'>{{ $errors->first('nom') }}</strong>
    </span>
    @endif

  <div class="form-group">
    <label for="prix_achat">Prix d'achat</label>
    <input type="text" class="form-control" name="prix_achat" id="prix_achat" placeholder="Prix d'achat" value="{{ $product->prix_achat }}">
  </div>
  @if ($errors->has('prix_achat'))
    <span class="help-block">
        <strong class='text-danger'>{{ $errors->first('prix_achat') }}</strong>
    </span>
  @endif

  <div class="form-group">
    <label for="prix_vente">Prix de vente</label>
    <input type="text" class="form-control" name="prix_vente" id="prix_vente" placeholder="Prix de vente" value="{{ $product->prix_vente }}">
  </div>
  @if ($errors->has('prix_vente'))
    <span class="help-block">
        <strong class='text-danger'>{{ $errors->first('prix_vente') }}</strong>
    </span>
  @endif
  
    <div class="form-group">
        <label for="famille">Famille</label>
        <select class="form-control" name="famille">
          @foreach($familles as $famille)
            <option {{ $product->famille_id == $famille->id ? 'selected="selected"' : '' }} value="{{ $famille->id }}">{{ $famille->nom }}</option>
          @endforeach
        </select>
      </div>

  <div class="form-group">
    <label for="prix_vente">Stock</label>
    <input type="text" class="form-control" name="stock" id="stock" placeholder="Stock" value="{{ $product->stock }}">
  </div>
  @if ($errors->has('stock'))
    <span class="help-block">
        <strong class='text-danger'>{{ $errors->first('stock') }}</strong>
    </span>
  @endif
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection