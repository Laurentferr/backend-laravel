@extends('layout')

@section('content')

<form class="form-horizontal" action="{{ url('famille/update/' . $famille->id) }}" method="POST">
@csrf
@method('PATCH')

    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" name="nom" id="nom" placeholder="Nom du produit" value="{{ $famille->nom }}">
    </div>

    @if ($errors->has('nom'))
        <span class="help-block">
            <strong class='text-danger'>{{ $errors->first('nom') }}</strong>
        </span>
    @endif

    <div class="form-group">
        <label for="slug">Slug</label>
        <input type="text" class="form-control" name="slug" id="slug" placeholder="slug" value="{{ $famille->slug }}">
    </div>
    @if ($errors->has('slug'))
        <span class="help-block">
            <strong class='text-danger'>{{ $errors->first('slug') }}</strong>
        </span>
    @endif
 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection