@extends('layout')

@section('content')
<div class="row text-center">
    <div class="col-10 justify-content-center mx-auto">
        <h1>Gestion des Rôles</h1>
        
        <table class="table table-responsive">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Nom</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">User</th>
                    <th scope="col">Admin</th>
                </tr>
            </thead>
            @foreach($users as $user)
            <tbody>
            <form action="roles/assign/{{ $user->id }}" method="post">
            @csrf
                <tr>
                    <td>
                    {{ $user->id }}
                    </td>
                    <td>{{ $user->firstname }}</td>
                    <td>{{ $user->lastname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <input type="checkbox" name="role_user" {{ $user->hasRole('User') ? 'checked' : ''}}>
                    </td>
                    <td>
                        <input type="checkbox"  name="role_admin" {{ $user->hasRole('Admin') ? 'checked' : ''}}>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-success btn-sm">
                            <i class="fa fa-btn fa-user"></i> Assigner <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
                        </button>
                    </td>
                </tr>
                </form>
            </tbody>
            @endforeach
        </table>
    </div>
    <br>
</div>

@endsection