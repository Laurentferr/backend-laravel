@extends('layout')

@section('content')
<div class="row text-center">
    <div class="col-12 col-sm-6 justify-content-center mx-auto">
        <h1>Familles</h1>
        <h3>Retrieve de la table 'familles'</h3>

        <table class="table table-responsive">
            <thead class="">
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Slug</th>
                </tr>
            </thead>
            <tbody>
                @foreach($familles as $famille)
                <tr>
                    <th>{{ $famille->id }}</th>
                    <td>{{ $famille->nom }}</td>
                    <td>{{ $famille->slug }}</td>
                    <td>
                        <a class="btn btn-danger btn-sm" href="famille/delete/{{ $famille->id }}" role="button" onclick="return confirm('Etes vous sûr de vouloir supprimer cette famille ?')">Supprimer<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                    <td>
                        <a class="btn btn-warning btn-sm" href="famille/edit/{{ $famille->id }}" role="button">Modifier<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="mx-auto">
            <a role="button" class="btn btn-success btn-sm" href="/famille/create">Ajouter une famille</a>
        </div>
    </div>
    <br>
    
</div>

@endsection