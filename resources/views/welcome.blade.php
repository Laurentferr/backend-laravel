@extends('layout')

@section('content')
<div class="row">

    <div class="col-12 text-center">

        

        <div class="card">
            <div class="card-header">
                <h1>Laravel - Back End</h1>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Migrations</li>
                <li class="list-group-item">Retrieve Simple</li>
                <li class="list-group-item">CRUD MVC</li>
                <li class="list-group-item">Seeding</li>
                <li class="list-group-item">Relation ONE TO MANY</li>
                <li class="list-group-item">Authentification</li>
            </ul>
            <div class="card-header">
                <h1>Liens utiles</h1>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><a target="_blank" href="https://laravel.com">Doc Laravel</a></li>
                <li class="list-group-item"><a target="_blank" href="https://www.w3schools.com/">w3schools</a></li>
                <li class="list-group-item"><a target="_blank" href="https://laravel.sillo.org/">Laravel Sillo</a></li>
                <li class="list-group-item"><a target="_blank" href="https://www.youtube.com/watch?v=igkr7EN9QuM&list=PLlxQJeQRaKDSnp4hbA3nJj_OnB0SCQLBU">Vidéos Teachers du Net</a></li>
            </ul>

        </div>

    </div>
    
</div>

@endsection
