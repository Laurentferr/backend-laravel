<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/simple-produit', 'ProductsController@index1')->name('simple-produit');

Route::get('/no_access', function () { return view('no_access');});

// CRUD Produits
Route::get('/products', ['uses' => 'ProductsController@index',
                        'middleware' => 'roles',
                        'roles' => ['Admin'],
                        'as' => 'products'
                        ]);

Route::get('/product/create', ['uses' => 'ProductsController@create',
                                'middleware' => 'roles',
                                'roles' => ['Admin']
]);
Route::post('/product/store', ['uses' => 'ProductsController@store',
                                'middleware' => 'roles',
                                'roles' => ['Admin']
]);
Route::get('/product/delete/{id}', ['uses' => 'ProductsController@destroy',
                                    'middleware' => 'roles',
                                    'roles' => ['Admin']
]);
Route::get('/product/edit/{id}', ['uses' => 'ProductsController@edit',
                                    'middleware' => 'roles',
                                    'roles' => ['Admin']
]);
Route::patch('/product/update/{id}', ['uses' => 'ProductsController@update',
                                    'middleware' => 'roles',
                                    'roles' => ['Admin']
]);

// CRUD Familles
Route::get('/familles', ['uses' => 'FamillesController@index',
                        'middleware' => 'roles',
                        'roles' => ['Admin'],
                        'as' => 'familles'
]);
Route::get('/famille/create', ['uses' => 'FamillesController@create',
                                'middleware' => 'roles',
                                'roles' => ['Admin']
]);
Route::post('/famille/store', ['uses' => 'FamillesController@store',
                                'middleware' => 'roles',
                                'roles' => ['Admin']
]);
Route::get('/famille/delete/{id}', ['uses' => 'FamillesController@destroy',
                                    'middleware' => 'roles',
                                    'roles' => ['Admin']
]);
Route::get('/famille/edit/{id}', ['uses' => 'FamillesController@edit',
                                'middleware' => 'roles',
                                'roles' => ['Admin']
]);
Route::patch('/famille/update/{id}', ['uses' => 'FamillesController@update',
                                    'middleware' => 'roles',
                                    'roles' => ['Admin']
]);

// Roles

Route::get('/roles', 'RolesController@index')->name('roles');

Route::post('roles/assign/{id}', 'RolesController@assignRoles');

// Routes Boutique

Route::get('/boutique', 'ProductsController@boutique')->name('boutique');
